#!/bin/sh

LIBDIR=/usr/lib/bbcloner
BINDIR=/usr/bin
MANDIR=/usr/share/man/man1/

if [ "$(id -u)" != "0" ]; then
    echo "you are not root. exiting..."
    exit 1
fi

echo "Uninstalling..."
rm -rf $LIBDIR/ 2>/dev/null || true
rm $BINDIR/bbcloner 2>/dev/null || true
rm $MANDIR/bbcloner.1 2>/dev/null || true

echo "Uninstalled."

#!/bin/sh

VERSION=$1
NAME=bbcloner
TARGET_DIR=releases/deb/$NAME-$VERSION

if [ -z $VERSION ]; then
    echo "Version required for this build rule" >&2
    exit
fi

if [ -d $TARGET_DIR ]; then
    rm -rf $TARGET_DIR
fi

# Create the release directory structure.
mkdir -p $TARGET_DIR
mkdir -p $TARGET_DIR/usr/lib/$NAME
mkdir -p $TARGET_DIR/usr/bin
mkdir -p $TARGET_DIR/usr/share/doc/$NAME
mkdir -p $TARGET_DIR/usr/share/man/man1

# Copy the source to the release directory structure.
cp LICENSE $TARGET_DIR/usr/share/doc/$NAME
cp README.md $TARGET_DIR/usr/share/doc/$NAME
cp doc/$NAME.1 $TARGET_DIR/usr/share/man/man1
cp src/bbcloner $TARGET_DIR/usr/lib/$NAME
cp -ar releases/cache/requests-1.2.0/requests $TARGET_DIR/usr/lib/$NAME/requests

ln -s ../lib/$NAME/$NAME $TARGET_DIR/usr/bin/$NAME
cp -ar contrib/debian/DEBIAN $TARGET_DIR/

# Remove some files / directories we don't want in the release.
find releases/ -name ".svn" -type d -print0 | xargs -0 /bin/rm -rf
find releases/ -name "*.pyc" -type f -print0 | xargs -0 /bin/rm -rf

# Bump version numbers
find releases/ -type f -print0 | xargs -0 sed -i "s/%%VERSION%%/$VERSION/g" 

fakeroot dpkg-deb --build $TARGET_DIR > /dev/null

